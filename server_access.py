#coding:utf-8
from flask import *
from werkzeug import *
import os
import sys
import requests
import base64
import json
#import pymongo
#import tarte_main
from tarte_main import *

UPLOAD_FOLDER = '~/pck/upload/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config["JSON_AS_ASCII"] = True
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER

HOST = "0.0.0.0"
PORT = 5000
#client = pymongo.MongoClient("localhost", 27017)
#db = client['pumpkin_tarte']

###   RaspberryPi(いえここ)との通信   ###

@app.route("/raspi/update_home/", methods=['POST'])
def ras_home():
#引数{beacon_id, in_home, raspi_num}
#    data = request.json
    beacon_id = request.json["beacon_id"]
    in_home = request.json["in_home"]
    raspi_num = request.json["raspi_num"]
#    print(data)
#    beacon_id = request.args.get("beacon_id")
#    in_home = request.args.get("in_home")
#    raspi_num = request.args.get("raspi_num")

    update_home(beacon_id, in_home, raspi_num)

    return "complete"

###   Androidとの通信   ###

@app.route("/init_famicoco/", methods=['GET'])
def init_famicoco():
#引数{user_name, notice, beacon_id, regi_id}
    name = request.args.get("user_name")
    notice = request.args.get("notice")
    if notice == None :
        notice = True
    if request.args.get("beacon_id") is None:
        beacon_id = None
    else :
        beacon_id = request.args.get("beacon_id")
    regi_id = request.args.get("regi_id")
    pict = request.args.get("pict_num")

    app_init(name, notice, beacon_id, regi_id, pict)
    return "complete"

@app.route("/user/pict/", methods=['GET'])
def pict():
    res = get_pict()
    logger.debug("pict: {}".format(res))
    return jsonify(res)

@app.route("/get_tl/", methods=['GET'])
def get_tl():
#引数なし
    tl_data = json.dumps(get_TL())
#    tl_data = get_TL()
    return tl_data

@app.route("/get_pet/", methods=['GET'])
def get_p():
    res = get_pet()
    logger.debug("get_pet: {}".format(res))
    return jsonify(res)

@app.route("/home/", methods=['GET'])
def home():
#引数なし
    res = get_home()
    logger.debug(res)
    return jsonify(res)

@app.route("/out/talk/hist/", methods=['GET'])
def get_out_hist():
    name = request.args.get("name")
    res = out_hist(name)
    logger.debug("out_hist:", res)
    return jsonify(res)

@app.route("/home/talk/", methods=['GET'])
def ac_home_talk_push():
    name = request.args.get("name")
    text = request.args.get("text")
    
    home_talk_push(name, text)
    return "complete"

#アプリでのibeacon受信
@app.route("/receive_beacon/", methods=['POST'])
def receive_beacon():
#    name = request.args.get("user_name")
#    logger.debug(request.form)
    #logger.info(requests.json)
    sender = request.form["sender"]
    beacon_id = json.loads(request.form["data"])
    logger.debug(json.loads(request.form["data"]))
    logger.debug("beacon_id: {}".format(beacon_id))
    res = update_out_group(beacon_id, sender)
    logger.debug("update: {}".format(res))
    return res

@app.route("/get_out/", methods=['GET'])
def outpo():
#引数{user_name}
    name = request.args.get("user_name")
    res = get_out(name)
    logger.debug("res: {}".format(res))
    return jsonify(res)

@app.route("/out/poyo/", methods=['POST'])
def out_talkp():
    logger.debug("bugdesu: {}".format(request.form))
    logger.debug("json/data: {}".format(request.form['data']))
    data = json.loads(request.form['data'])
    member = data[0]
    name = data[1][0]
    text = data[1][1]
    logger.debug("name: {}, text: {}".format(name, text))
    out_talk_push(member, name, text)
    return "complete"

@app.route("/out/talk/", methods=['POST'])
def out_talk():
    logger.debug("poyopoyo: {}".format(request.form))
    member = json.loads(request.form["data"])
    res = get_out_talk(member)
    logger.debug("out_talk_debug: {}".format(res))
    return jsonify(res)

@app.route("/setting/notice/update/", methods=['POST'])
def ac_setting_notice():
    notice = json.loads(request.form["data"])
    name = json.loads(request.form["name"])
    update_setting_notice(name, notice)
    
    return "complete"

@app.route("/setting/notice/", methods=['GET'])
def setting_notice():
    return get_setting_notice(notice)

@app.route("/raspi/pet/play/", methods=['POST'])
def petplay():
    logger.debug("pet json: {}".format(request.json["beacon_id"]))
    beacon_id = request.json["beacon_id"]
    res = pet_play(beacon_id)

    logger.debug("pet: {}".format(res))
    return res

@app.route("/raspi/pet/walk/", methods=['GET'])
def petwalk():
    pet_walk()

@app.route("/raspi/pet/is_nohome/", methods=['GET'])
def is_nohome():
    is_nohome_now()

# @app.route("/get_out/talk/", methods=['GET'])
# def out_talk():
# #引数{user_name}
#
# @app.route("/user_setting/", methods=['GET'])
# def user_setting():
# #引数{user_name}
#
# @app.route("/user_setting/", methods=['POST'])
# def user_setting():
# #引数{user_name, notice}



if __name__ == "__main__":
  app.run(HOST, PORT, debug=True)
