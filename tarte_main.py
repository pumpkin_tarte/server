#from pymongo import MongoClient
import sys
import pymongo
import server_ope
from datetime import datetime
from logging import getLogger, StreamHandler, DEBUG
import json
import requests

logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)

client = pymongo.MongoClient("localhost", 27017)
famicoco_db = client['u-22']

usercol = famicoco_db['user']
#{"name":"nemu", "icon":"pict", "notice":"True or False", "beacon_id":"uuid", "regi_id":"regi_id"}
homecol = famicoco_db['home']
home_lastcol = famicoco_db['home_last']
#{"member":{"nemu", "kurokoji", "mito"}, "talk":{"name":"nemu", "text":"poyo"}}
outcol = famicoco_db['out']
out_talkcol = famicoco_db['out_talk']

tlcol = famicoco_db['timeline']
#{"name":"nemu", "action":act_num, "time":"time"}
#user_where  = famicocodb.['user_where']
#{"user_name":"nemu", "in_home":True:False}
db_ope = server_ope.famicocoDBclass()

def app_init(name, notice, beacon_id, regi_id, pict):
#    if usercol.find_one({"name":name}) != None:
#        user = usercol.find_one({"name":name})

#    notice = user["notice"]
#    beacon_id = user["beacon_id"]
#    regi_id = user["regi_id"]
    db_ope.user_make(name, notice, beacon_id, regi_id, pict)
#     user = {mem["name"] for mem in usercol.find()} - {"しゅり", "しゅうき", "お父さん", "お母さん", "かん爺"}
    user = {mem["name"] for mem in usercol.find()}
    if len(user):
        before = outcol.find_one({"main_out":{"$exists":True}})["main_out"]
        outcol.update({"main_out":{"$exists":True}}, {"main_out":list(user)}, upsert=True)
        outcol.insert({"group":[name]})
        # outcol.update({"group":{"$in":list(user)}}, {"group":list(user)}, upsert=True)

def get_pict():
    pict_list = []
    user_list = []
    for mem in usercol.find():
        user_list += [mem["name"]]
    user_list = set(user_list) - {"しゅり", "しゅうき", "お父さん", "お母さん", "かん爺"}
    for mem in user_list:
        conf = usercol.find_one({"name":mem})
        pict_list += [[conf["name"], conf["pict"]]]
    return pict_list

def update_setting_notice(name, notice):
    for i, user in enumerate(name):
        user_con = usercol.find_one({"name":name})
        usercol.update({"name":name}, {"name":name, "regi_id":user_con["regi_id"], "notice":notice, "beacon_id":user_con["beacon_id"]})

def get_setting_notice(notice):
    return db_ope.get_setting_notice(notice)

def get_TL():
    return db_ope.get_TL()

def get_pet():
    pet_ALL = []
    for pet_po in tlcol.find({"pet":{"$exists":True}}):
        pet_ALL += [pet_po["action"]]
    logger.debug(pet_ALL)
    return pet_ALL

def get_home():
    return db_ope.get_home()

def home_talk_push(name, text):
    db_ope.home_talk_push(name, text)
    home_member = homecol.find_one({"main_home":{"$exists":True}})["main_home"]
    home_member.remove(name)
    regi_id = []
    for name in home_member:
        regi_id.append(usercol.find_one({"name":name})["regi_id"])
    push(regi_id, text)

def update_home(beacon_id, in_home, raspi_num):
    if usercol.find_one({"beacon_id":beacon_id}) == None:
        print("None")
        return
    name = usercol.find_one({"beacon_id":beacon_id})["name"]
    db_ope.update_home_db(name, in_home, raspi_num)

    if homecol.find_one({"main_home":{"$in":[name]}}) == None:
        text = name + "が外出しました。"
        action_num = 2
    else :
        text = name + "が帰宅しました。"
        action_num = 1

    tlcol.insert({"time":str(datetime.now()), "action":text, "action_num":action_num})

#beacon_id = {bid for bid in beacon_id if usercol.find_one({"beacon_id":bid}) != None}
    if usercol.find_one({"name":name})["notice"]:
        regi_id = [regid["regi_id"] for regid in usercol.find() if regid["regi_id"] != None and regid["name"] != name]
#        text = tlcol.find({"action":{"$exists":True}}).sort({-1})[0]
        push(regi_id, text)
    outcol.remove({"group":{"$in":[name]}})


def get_out(name):
    return db_ope.get_out(name)

#def out_hist(name):
#    out_hist_group = []
#    for hist_group in 

def get_out_talk(member):
    member = sorted(member)
    if out_talkcol.find_one({"group":member}) is None:
        out_talkcol.insert({"group":member, "talk":[]})
    out_talk_ALL = []
    for out_list in out_talkcol.find_one({"group":member})["talk"]:
        out_talk_ALL += [out_list]
    return out_talk_ALL

def out_talk_push(member, name, text):
    member = sorted(member)
    if out_talkcol.find_one({"group":member}) is None:
        out_talkcol.insert({"group":member, "talk":[]})
    out_talk_list = []
    out_talk_list = out_talkcol.find_one({"group":member})["talk"]
    out_talk_list += [{"name":name, "text":text, "time":str(datetime.now())}]
    out_talkcol.update({"group":member}, {"group":member, "talk":out_talk_list})
    regi_id = []
    for mem in member:
        regi_id += [usercol.find_one({"name":mem})["regi_id"]]
    push(regi_id, text)
    return "complete"

def update_out_group(beacon_id, sender):
    beacon_id = {bid for bid in beacon_id if usercol.find_one({"beacon_id":bid}) != None}
    if beacon_id is None:
        return json.dumps({"result": "failed"})
    logger.debug("bid: {}".format(beacon_id))
    member = []
    for bid in beacon_id:
        name = usercol.find_one({"beacon_id":bid})
        logger.debug("name: " + name["name"])
        if outcol.find_one({"main_out":{"$in":[name["name"]]}}) is None:
            return json.dumps({"new_result": "bad"})
        else :
            member.append(name["name"])
    logger.debug("log::::::::::::{}".format(member))
    old_member = dict()
    # if outcol.find_one({"group":{"$in":member}}) is None:
    if outcol.find_one({"group":{"$in":[sender]}}) is None:
        outcol.insert({"group":member})
        old_member = []
    else :
        old_member = outcol.find_one({"group":{"$in":[sender]}})["group"]
    outcol.remove({"group":{"$in":member}})
    outcol.insert({"group":member})
    logger.debug("dump::{}".format(old_member))
    if member is None:
        return jsonify({"old_result": "bad"})

    db_ope.update_out_group(member)
    #miss_member = []
    #new_member = []
    #for miss in set(old_member) - set(member):
    #    miss_member.append(miss)
    #for new in set(member) - set(old_member):
    #    new_member.append(new)
    new_member = list(set(member) - set(old_member))
    miss_member = list(set(old_member) - set(member))
    logger.debug("member: {}".format(member))
    logger.debug("miss_member: {}".format(miss_member))
    logger.debug("new_member: {}".format(new_member))

    ###home_member = homecol.find_one({"main_home":{"$exists":True}})["main_home"]
    ###regi_id = []
    ###for name in home_member:
    ###    regi_id.append(usercol.find_one({"name":name})["regi_id"])
    ###push(regi_id, text)
    for miss in miss_member:
        if miss != sender:
            text = miss + "が" + sender + "と離れました。"
            outcol.insert({"group":[miss]})
            tlcol.insert({"action_num":4, "action":text, "time":str(datetime.now())})
            if usercol.find_one({"name":miss})["notice"]:
                regi_id = [regid["regi_id"] for regid in usercol.find() if regid["regi_id"] != None]
                push(regi_id, text)
    if len(new_member) >= 1:
        for new in new_member:
            outcol.remove({"group":[new]})
            if new != sender:
                text = new + "が" + sender + "と一緒にいます。"
                tlcol.insert({"action_num":3, "action":text, "time":str(datetime.now())})
                if usercol.find_one({"name":new})["notice"]:
                    regi_id = [regid["regi_id"] for regid in usercol.find() if regid["regi_id"] != None]
                    push(regi_id, text)

    outcol.remove({"group":[]})
    return "complete"

def pet_play(beacon_id):
    if usercol.find_one({"beacon_id":beacon_id}) is None:
        return "だれ"
    name = usercol.find_one({"beacon_id":beacon_id})["name"]
    text = name + "だ！遊んで！遊んで！"
    tlcol.insert({"pet":True, "action":text, "time":str(datetime.now()), "action_num":5})
    regi_id = [regid for regid in usercol.find() if regid["regi_id"] != None]
    push(regi_id, text)
    return "complete"

def pet_walk():
    text = "散歩つれてって～"
    regi_id = [regid for regid in usercol.find() if regid["regi_id"] != None]
    tlcol.insert({"pet":True, "action":text, "time":str(datetime.now()), "action_num":5})
    push(regi_id, text)

def is_nohome_now():
    if homecol.find_one({"main_home":{"$exists":True}}) == []:
        text = "つまんないなあ、誰か構ってくれないかなあ。"
        tlcol.insert({"pet":True, "action":text, "time":str(datetime.now()), "action_num":5})
        regi_id = [regid for regid in usercol.find() if regid["regi_id"] != None]
        push(regi_id, text)


def push(regi_id, text):
    logger.info(regi_id)
#    regi_id.remove(None)

    fcm_url = "https://fcm.googleapis.com/fcm/send"
    key = "key=" + "AIzaSyD4sn4mc8HhSWPVACcLjUvzHAlbKpqix0s"
    headers = {
        'content-type' : 'application/json',
        'Authorization' : key
    }
    data = {
        'registration_ids' : regi_id,
        'data' : {
            'title' : 'ふぁみここ',
            'body' : text
        },
        'priority' : 'high'
    }

    requests.post(fcm_url, data=json.dumps(data), headers=headers)


#def user_config(name, flag):
#    if flag:
#    else :
#        user = usercol.find_one({"user_name":name})
#        del user["_id"]
#        return user
