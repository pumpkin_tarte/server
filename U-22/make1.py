import time
from tarte_main import *

user_list = ["しゅり", "しゅうき", "お父さん", "お母さん", "かん爺"]
bid_list = ["mosmos_syrc", "RINPANMAN0606", "mitohato14", "10riridk0", "m_kyoujyu"]
regi_id = [0, 1, 2, 3, 4]
pict = [0, 1, 2, 3, 4]


def sceen1():
    for i in range(4):
        print(i, "hoge")
        time.sleep(0.5)
        update_home(bid_list[3], True, 1)
        tlcol.insert({"pet":True, "action":"お腹すいたよお", 
            "time":str(datetime.now()), "action_num":5})
        update_home(bid_list[3], False, 1)
        update_home(bid_list[2], True, 1)
        tlcol.insert({"action_num": 4, "action": "しゅうきがかん爺と離れました",
            "time": str(datetime.now())})
        update_home(bid_list[2], False, 1)


def sceen2():
    for bid in bid_list:
        update_home(bid, False, 1)
    update_home(bid_list[3], True, 1)
    update_home(bid_list[1], True, 1)
    update_home(bid_list[4], True, 1)


def sceen3():
    # アプリからメッセージが送られる
    pass


def sceen4():
    update_home(bid_list[2], True, 1)
    time.sleep(1)
    update_home(bid_list[4], False, 1)


def sceen5():
    for bid in bid_list:
        update_home(bid, True, 1)
    update_home(bid_list[1], False, 1)
    update_home(bid_list[3], False, 1)
    update_home(bid_list[4], False, 1)
    update_out_group([bid_list[1], bid_list[3]])


def sceen6():
    # アプリ操作
    pass


def sceen7():
    # アプリ操作
    pass

def sceen8():
    update_out_group([bid_list[3]])


def sceen9():
    pass


def pet():
    for i in range(5):
        tlcol.insert({"pet":True, "action":"散歩つれてって～", 
            "time":str(datetime.now()), "action_num":5})
        tlcol.insert({"pet":True, "action":"お腹すいたよお", 
            "time":str(datetime.now()), "action_num":5})


def main():
    f = [sceen1, sceen2, sceen3, sceen4, sceen5, sceen6, sceen7, sceen8]
    print("play sceens value 1 to 8 (ex: 1 3 4)")
    play = [int(i) for i in input().split()]
    print("interval 9 values (ex: 1 2 1 1 3...)")
    interval = [int(i) for i in input().split()]

    for i, x in enumerate(play):
        f[x]()
        time.sleep(interval[i])

    pet()
    time.sleep(2)

    tlcol.insert({"pet":True, "action":"散歩つれてって～", 
        "time":str(datetime.now()), "action_num":5})
    sceen9()


if __name__ == '__main__':
    main()
