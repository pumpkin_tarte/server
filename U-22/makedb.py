import os
import json
from logging import getLogger, StreamHandler, DEBUG
from pymongo import MongoClient
from tarte_main import *

logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)

db_name = 'tarte'
collection_list = ['user', 'home', 'home_last', 'out', 'out_talk', 'timeline']
user_list = ["しゅり", "しゅうき", "お父さん", "お母さん", "かん爺"]
bid = ["mosmos_syrc", "RINPANMAN0606", "mitohato14", "10riridk0", "m_kyoujyu"]
regi_id = [0, 1, 2, 3, 4]
pict = [0, 1, 2, 3, 4]

client = MongoClient('localhost', 27017)
db = client[db_name]

def make_collections():
    for col in collection_list:
        db.create_collection(col)

def init_collections():
    db['out'].insert({"main_out": []})
    db['home'].insert({"main_home": []})
    db['home'].insert({"raspi_num":1, "member":list(new_home)})
    db['home'].insert({"raspi_num":2, "member":list(new_home)})

def init_users():
    for user, i in enumerate(user_list):
        app_init(user, True, bid[i], regi_id[i], pict[i])

def main():
    make_collections()

if __name__ == '__main__':
    main()

