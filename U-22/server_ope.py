#from pymongo import MongoClient
import pymongo
import json
import sys
from datetime import datetime
from logging import getLogger, StreamHandler, DEBUG

logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)

client = pymongo.MongoClient("localhost", 27017)
famicoco_db = client['tarte']

usercol = famicoco_db['user']
#{"name":"nemu", "notice":"True or False", "beacon_id":"uuid", "regi_id":"regi_id"}
homecol = famicoco_db['home']
home_lastcol = famicoco_db['home_last']
#{ 'count':num, 'last_time':time, 'name':'name'}}
#{
#    "main_home":[member]
#    "raspi":["raspi_num":raspi_num, "member":["nemu", "kurokoji", "mito"]]
#}
home_talkcol = famicoco_db['home_talk']
#{"name":"nemu", "text":"poyo", "time":time}
outcol = famicoco_db['out']
out_talkcol = famicoco_db['out_talkcol']
#{"member":{"nemu", "kurokoji", "mito"}, "talk":{"name":"nemu", "text":"poyo"}}

tlcol = famicoco_db['timeline']

#user_where  = famicocodb.['user_where']
#{"user_name":"nemu", "in_home":True:False}

class famicocoDBclass:

    def user_make(self, name, notice, beacon_id, regi_id, pict):
        if beacon_id is None:
            member = []
            if outcol.find_one({"main_out":{"$exists":True}}) is None:
                logger.debug("none_main_out")
            else :
                member = outcol.find_one({"main_out":{"$exists":True}})["main_out"]
            outcol.update({"main_out":{"$exists":True}}, {"main_out":member + [name]}, upsert=True)
        usercol.insert_one({'name':name, 'notice':notice, 'beacon_id':beacon_id, 'regi_id':regi_id, 'pict':pict})
        tlcol.insert({'action':str(name) + 'がふぁみここをはじめました。', 'time':str(datetime.now()), 'action_num':0})

    def get_TL(self):
        TLall = []
        for TLlist in tlcol.find().limit(500).sort('time", -1'):
            del TLlist['_id']
            TLlist['time'] = TLlist['time'][11:16]
            TLall.append(TLlist)
        return TLall

    def get_home(self):
        HOME_MEMBER = homecol.find_one({"main_home":{"$exists":True}})["main_home"]
        HOME_TALK_ALL = []
        for home_talk in home_talkcol.find().limit(500).sort("time", -1):
            del home_talk["_id"]
            home_talk["time"] = home_talk["time"][11:16]
            HOME_TALK_ALL.append(home_talk)
        RETURN_ALL = [HOME_MEMBER, HOME_TALK_ALL]
        return RETURN_ALL

    def home_talk_push(self, name, text):
        home_talkcol.insert({"name":name, "text":text, "time":str(datetime.now())})
        return

    #家にいる, いないの更新
    def update_home_db(self, name, in_home, raspi_num):
        all_member = set()
        for user in usercol.find():
            po = {str(user["name"])}
            all_member = all_member.union(po)
        if homecol.find_one({"raspi_num":raspi_num}) == None:
            old_home = set()
            new_home = set()
        else :
            old = homecol.find_one({"raspi_num":raspi_num})
            old_home = set(old["member"])
            new_home = old_home
        if in_home:
            new_home = new_home.union({name})
        else :
            new_home = new_home.difference({name})

        if homecol.find({"raspi_num":raspi_num}) == None:
            homecol.insert({"raspi_num":raspi_num, "member":list(new_home)})
        else :
            homecol.update({"raspi_num":raspi_num}, {"raspi_num":raspi_num, "member":list(new_home)}, upsert=True)
        if homecol.find_one({"main_home":{"$exists":True}}) == None:
            old_main = set()
        else :
            old_main = homecol.find_one({"main_home":{"$exists":True}})
        member = set()
        for rasmem in homecol.find({"raspi_num":{"$exists":True}}):
            member = member.union(rasmem["member"])

        if homecol.find({"main_home":{"$exists":True}}) == None:
            homecol.insert({"main_home":[{}]})
        homecol.update({"main_home":{"$exists":True}}, {"main_home":list(member)}, upsert=True)
        if outcol.find_one({"main_out":{"$exists":True}}) == None:
            outcol.insert({"main_out":[{}]})
        old_out = outcol.find_one({"main_out":{"$exists":True}})
        outcol.update({"main_out":list(old_out["main_out"])}, {"main_out":list(all_member - member)}, upsert=True)
        for old in outcol.find({"group":{"$in":[name]}}):
            outcol.update({"group":[name]}, {"group":list(set(old["group"]) - {name})})
        outcol.remove({"group":[]})
        #homecol.update({"name":name}, {"in_home":in_home})

#        for ras in homecol.find({"raspi_num":{"$exists":True}}):
#            if ras["member"] == []:
#                homecol.remove({"raspi_num":ras["raspi_num"]})

    def get_out(self, name):
        if outcol.find_one({"main_out":{"$exists":True}}) is None:
            main_out = []
            OUT_ALL = []
        else :
            main_out = outcol.find_one({"main_out":{"$exists":True}})["main_out"]
            OUT_ALL = [main_out]
        for out_list in outcol.find({"group":{"$exists":True}}):
            OUT_ALL += [out_list["group"]]
        logger.debug("ope out_all: {}".format(OUT_ALL))
        return OUT_ALL

    def update_out_group(self, member):
        ###groupの情報を持ったほうがよさそう###後でもともといるmember以外の値(ドキュメント)をとりたいから
        ###mem = outcol["member"]
        #old_group = outcol["group"]
        #if ()
        #for old in outcol.find({"group":{"$exists":True}}):

        #outcol.update({"group":{"$in":[member]}}, {"member":[member]}, upsert=True)
        outcol.update({"group":{"$in":member}}, {"group":member}, upsert=True)


def main():
    db = famicocoDBclass

if __name__ == '__main__':
    main()
