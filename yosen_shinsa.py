import os
import json
from logging import getLogger, StreamHandler, DEBUG
from pymongo import MongoClient
from tarte_main import *

logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)

db_name = 'u-22'
collection_list = ['user', 'home', 'home_last', 'out', 'out_talk', 'timeline']
user_list = ["しゅうき", "お母さん"]
bid = ["22", "003B"]
regi_id = [0, 1]
pict = [0, 1]

client = MongoClient('localhost', 27017)
db = client[db_name]

def make_collections():
    for col in collection_list:
        db.create_collection(col)

def init_collections():
    db['out'].insert({"main_out": []})
    db['home'].insert({"main_home": []})
    db['home'].insert({"raspi_num":1, "member":[]})
    db['home'].insert({"raspi_num":2, "member":[]})

def init_users():
    for i, user in enumerate(user_list):
        if i == 1:
            break
        app_init(user, True, bid[i], regi_id[i], pict[i])

def main():
    make_collections()
    init_collections()
    init_users()

if __name__ == '__main__':
    main()

