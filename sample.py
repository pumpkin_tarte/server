from flask import Flask, request, jsonify
import json
import pymongo
app = Flask(__name__)

HOST = "0.0.0.0"
PORT = 5000
client = pymongo.MongoClient("localhost", 27017)
db = client['test1']

@app.route("/")
def index():
  s = request.args.get('str')
  print(s)
  return s + ' hello'

@app.route("/init")
def init():
  col = db['user']
  col.insert({"name":"hoge", "beaconid":"poyo"})

  for data in col.find():
    print(data)
    del data["_id"]
#    return data
#    res = json.dumps(data)
    return json.dumps(data)

@app.route("/hello")
def hello():
  return "Hello, World!"

if __name__ == "__main__":
  app.run(HOST, PORT, debug=True)
